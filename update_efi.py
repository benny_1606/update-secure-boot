#!/bin/python3
from pathlib import Path, PosixPath
from pprint import pprint
from filecmp import cmp
import time, subprocess

file_list = []

img_list = sorted(Path('/boot').glob('initramfs-*'))
vm_list = sorted(Path('/boot').glob('vmlinuz-*'))
intel_list = sorted(Path('/boot').glob('intel-ucode.img'))

file_list.append(img_list)
file_list.append(vm_list)
file_list.append(intel_list)

# pprint.pprint(file_list)

sign_needed = False

for item in file_list:
    if sign_needed == False:
        for file in item:
            efi_path = PosixPath('/efi', file.name)
            
            if efi_path.exists() == False:
                sign_needed = True
                break
            
            if cmp(file, efi_path) == False:
                print(str(file) + ", " + str(efi_path) + " are DIFFERENT")
                sign_needed = True
                break
            else:
                print(str(file) + ", " + str(efi_path) + " are same")

if sign_needed:
    print("\nSIGN NEEDED")
    subprocess.run("/root/sgrub/gen_all.sh", shell=True, check=True)
else:
    print("\nSign not needed")

#input("=== PRESS ENTER TO EXIT ===")
