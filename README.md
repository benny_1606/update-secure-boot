# Update Secure Boot

# Problem Statement
You have setted up [Secure Boot and Secure Grub2]. 

You have also setted up a [alpm-hooks] ([ArchWiki][pacman_hook_archwiki]), which automatically signed the `/boot` after installed/updated some Linux Kernel.

However, there are always some *(for crying out loud)* packages updates that somehow change the files in `/boot`. 

Problem: AND, those packages do not trigger your alpm-hooks, as I *really* do not know what packages will modifies files in the `/boot` the first place.

The result: As what you expect, when *those* packages require the new version of kernel and in fact it is not. Error EVERYWHERE.

Also, I do not want to sign `/boot` for *every* package update, even the `/boot` is not changed.

As a added benefit, I can now know **WHAT** package will modify `/boot` and caused the **All** the above problems.

# Requirement
Exact setup from [Secure Boot and Secure Grub2]

Note: This repo added the signing of `intel_ucode.img`.

# Usage
1. Change the `CHANGE_ME` in all files.
	* `grep -r CHANGE_ME .` can help
	
			./cfg.sh:GPG_KEY='CHANGE_ME'
			./gen_all.sh:cd CHANGE_ME
			./grub-initial.cfg:password_pbkdf2 root CHANGE_ME
			./grub-initial.cfg:search --no-floppy --fs-uuid --set=root CHANGE_ME
			./gen_standalone_bin.sh:GPG_KEY='CHANGE_ME'
			./gen_standalone_bin.sh:TARGET_EFI='CHANGE_ME'
			./gen_standalone_bin.sh:SECUREBOOT_DB_KEY='CHANGE_ME'
			./gen_standalone_bin.sh:SECUREBOOT_DB_CRT='CHANGE_ME'

	
2. Create trigger for executing `update_efi.py` after package update. (See [ArchWiki][pacman_hook_archwiki] for ArchLinux)

# License
MIT License

Copyright (c) 2020 Adam S


[Secure Boot and Secure Grub2]: https://ruderich.org/simon/notes/secure-boot-with-grub-and-signed-linux-and-initrd
[alpm-hooks]: https://jlk.fjfi.cvut.cz/arch/manpages/man/alpm-hooks.5
[pacman_hook_archwiki]: https://wiki.archlinux.org/index.php/Pacman#Hooks